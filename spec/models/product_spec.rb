RSpec.describe "Product_model", type: :model do
  describe 'page_related_products' do
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxon2) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product2) { create(:product, taxons: [taxon2]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
    let!(:page_related_products) { Spree::Product.page_related_products(product) }

    it '関連商品に同じ商品は表示しない' do
      expect(page_related_products).not_to include product
    end

    it '関連商品に関連しない商品は表示しない' do
      expect(page_related_products).not_to include product2
    end

    it '関連商品が表示される' do
      related_products.each do |related_product|
        expect(page_related_products).to include related_product
      end
    end
  end
end
