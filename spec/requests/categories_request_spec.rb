RSpec.describe "Categories_request", type: :request do
  describe 'showメソッド動作確認テスト' do
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxon2) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product2) { create(:product, name: 'product2', taxons: [taxon2]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it '200が返ってくる' do
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end

    it '意図したビューがレンダリングされている' do
      expect(response).to render_template :show
    end

    it '@taxonのインスタンス変数が適切である' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@taxonomyのインスタンス変数が適切である' do
      expect(assigns(:taxonomies)).to contain_exactly taxonomy
    end

    it '@productのインスタンス変数が適切である' do
      expect(assigns(:products)).to contain_exactly product
    end

    it 'taxonに紐づかない商品データの非表示テスト' do
      expect(response.body).not_to eq product2.name
    end
  end
end
