RSpec.describe "Products_request", type: :request do
  describe "showメソッド動作確認テスト" do
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let(:max_relation_products) { Potepan::ProductsController::MAX_RELATION_PRODUCTS }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, max_relation_products, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it '200が返ってくる' do
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end

    it '意図したビューがレンダリングされている' do
      expect(response).to render_template :show
    end

    it '@productのインスタンス変数が適切である' do
      expect(assigns(:product)).to eq product
    end

    it '@related_productsのインスタンス変数が適切である' do
      expect(assigns(:related_products)).to match_array related_products
    end

    it '関連商品の表示テスト' do
      expect(response.body).to include related_products.first.name
      expect(response.body).to include related_products.first.display_price.to_s
    end
  end
end
