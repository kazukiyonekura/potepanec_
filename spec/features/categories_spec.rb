RSpec.feature "categories", type: :feature do
  describe 'categories_show_test' do
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxon2) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product2) { create(:product, name: 'product2', taxons: [taxon2]) }
    let!(:bags) { create(:taxon, name: 'Bags', taxonomy: taxonomy, parent: taxonomy.root) }

    before do
      visit potepan_category_path(id: taxon.id)
    end

    it 'showページの商品名、商品説明、商品価格の表示テスト(存在確認)' do
      expect(page).to have_content taxon.name
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end

    it '商品詳細ページのリンクが正常に機能している' do
      expect(page).to have_link product.name
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it 'サイドバー(taxonomy.name)表示テスト' do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
      end
    end

    it 'サイドバーのリンクが正常に機能している' do
      within ".side-nav" do
        expect(page).to have_link 'Bags'
        click_link 'Bags'
        expect(current_path).to eq potepan_category_path(bags.id)
      end
    end
  end
end
