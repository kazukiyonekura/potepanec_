RSpec.feature "products", type: :feature do
  describe 'products_show_test' do
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let(:taxon2) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product2) { create(:product, taxons: [taxon2]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before  do
      visit potepan_product_path(product.id)
    end

    it 'showページの商品名、商品説明、商品価格の表示テスト(存在確認)' do
      expect(page).to have_content product.name
      expect(page).to have_content product.description
      expect(page).to have_content product.display_price
    end

    it '一覧ページへのリンクが正常に機能している' do
      expect(page).to have_link "一覧ページへ戻る"
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it '関連商品のリンクが正常に機能している' do
      related_products.each do |related_product|
        expect(page).to have_link related_product.name
        click_link related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end
  end
end
