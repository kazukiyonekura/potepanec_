class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.all.includes(:taxons)
    @products = Spree::Product.in_taxons(@taxon).includes(master: [:images, :default_price])
  end
end
