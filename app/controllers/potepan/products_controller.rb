class Potepan::ProductsController < ApplicationController
  MAX_PRODUCT_SQL = 10
  MAX_RELATION_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products =
      Spree::Product.page_related_products(@product).
        includes(master: [:images, :default_price]).
        limit(MAX_PRODUCT_SQL).
        sample(MAX_RELATION_PRODUCTS)
  end
end
